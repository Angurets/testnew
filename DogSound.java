package Animal;

public class DogSound implements AnimalSound {

    public void ShowSound() {
        System.out.println("A dog says bow-wow-wow");
    }
}